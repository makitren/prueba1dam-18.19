/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolgenealogico;

/**
 *
 * @author 1DAM
 */
//con esta clase ,en un main, crear 3 generaciones de persona,
//Abuelo1, Abuela1, Abuelo2, Abuela2, Abuelo3, Abuela3
//    |       |         |       |        |      |
// ----------------    -----------    --------------
//         |                |                |
//      Madre 2       Padre2  Madre2      Padre1
//         |              |     |            |
//         ---------------       -------------
//         |             |             |
//       Hija2          Hijo2         Hijo1
//Construir este arbol genealógico usando la clase Persona 
//y hacer sout(Hijo1.devuelveMadre().devuelvePareja().devuelvePadre());


public class Persona {
    //Añadir a cada persona un padre y una madre
    //Añadir a cada persona una pareja
    //Añadir a cada persona un array de hijos
    
    
    String nombre;
    String primerApellido;
    String segundoApellido;
    Persona madre;
    Persona pareja;
    Persona padre;
    boolean sexo;//true femenino false masculino
    Persona[]hijo;
    
    public Persona(String nombre, String primerApellido, String segundoApellido,boolean sexo, Persona madre,Persona[]hijo){
        this.nombre=nombre;
        this.primerApellido=primerApellido;
        this.segundoApellido=segundoApellido;
        this.sexo=sexo;
        this.madre=madre;
        this.hijo=hijo;
    }
    
}
