/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema4;

/**
 *
 * @author 1DAM
 */
public class Persona {

    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
     Vaca[]vacas;
    
    public void quedarmeVacas(Vaca[] vac){
        this.vacas=new Vaca[vac.length];
        int contador=0;
        for (int i=0;i<vac.length;i++){
        if(this.puesto.equals(vac[i].funcion)){
        vacas[contador]=vac[i];
        contador++;
 }
            
}
    }
    public void enseñarVacas(){
        System.out.println("Las vacas que tiene "+this.nombre+ "son:");
        for (int x=0;x<vacas.length;x++){
            if(this.vacas[x]==null){
                break;
            }else{
                System.out.println(this.vacas[x].nombre);
            }
        }
            }
    

//3
    public String cuantoGana() {
        return "Gana " + sueldo + " €";
    }

    public float proporcionSueldo(Persona personalizado) {
        return personalizado.sueldo / this.sueldo;
    }

    public float ajustaSueldoA(Persona personalizado1) {
        if (this.puesto.equals("ordeño")) {
            if (personalizado1.puesto.equals("gerencia")) {
                this.sueldo = (float) 0.50 * personalizado1.sueldo;
            }
            if (personalizado1.puesto.equals("carniceria")) {
                this.sueldo = (float) 1.25 * personalizado1.sueldo;
            }
        }else{
            if (this.puesto.equals("carniceria")) {
            if (personalizado1.puesto.equals("gerencia")) {
                this.sueldo = (float) 0.25 * personalizado1.sueldo;
            }
            if (personalizado1.puesto.equals("ordeño")) {
                this.sueldo = (float) 0.75 * personalizado1.sueldo;
            }
            }else{
                if (this.puesto.equals("gerencia")) {
            if (personalizado1.puesto.equals("ordeño")) {
                this.sueldo = (float) 1.50 * personalizado1.sueldo;
            }
            if (personalizado1.puesto.equals("carniceria")) {
                this.sueldo = (float) 1.75 * personalizado1.sueldo;
            }
            }
            }
        }
        return this.sueldo;
        }
    
    
    

    public String imprimePersona() {
        return nombre + ":" + apellidos + ":" + this.edad + ":" + this.altura;   //utilizar this cuando se quiera. Es mejor usarla para dar claridad
    }

    /**
     * Dice si la persona que llama a la funcion es mayor de una cierta edad
     *
     * @param int edad es la edad con la que vamos a comparar a la persona
     * @return true si la persona es mayor, false si la persona es menor o de
     * igual edad
     */
    public boolean esMayorDe(int edad) {
        return this.edad > edad;
    }
}
