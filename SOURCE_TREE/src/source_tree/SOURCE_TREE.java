/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source_tree;

import java.util.Scanner;

import java.util.Scanner;
/**
 *
 * @author 1DAM
 */
public class SOURCE_TREE {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s=new Scanner(System.in);
        System.out.println("Numero de columnas");
        int columnas=s.nextInt();
        System.out.println("Numero de filas");
        int filas=s.nextInt();
        int[][]matriz=new int[filas][columnas];
        SOURCE_TREE.rellenar(matriz);
        SOURCE_TREE.rellenarAlReves(matriz);
        SOURCE_TREE.rellenarMult3(matriz);
        
    }
    public static void rellenar(int[][]matriz){
        
        for(int c=0;c<matriz.length;c++){
            for(int b=0;b<matriz[0].length;b++){
                matriz[c][b]=b+c*matriz[c].length;
                
            }
        }
        SOURCE_TREE.imprimir(matriz);
  
    }
        public static void rellenarAlReves(int[][]matriz){
                 
        for(int c=0;c<matriz.length;c++){
            for(int b=0;b<matriz[c].length;b++){
                matriz[matriz.length-c-1][matriz[c].length-b-1]=b+c*matriz[c].length;
                
            }
            
        }
        SOURCE_TREE.imprimir(matriz);
        }   
    public static void rellenarMult3(int[][]matriz){
        
        for (int i=0;i<matriz.length;i++){
            for (int j=0;j<matriz[0].length;j++){
                    matriz[i][j]=(j+i*matriz[i].length)*3;   
                }
        }
        SOURCE_TREE.imprimir(matriz);
    }
    public static void imprimir(int [][]matriz){
        System.out.println("");
        for (int i=0;i<matriz.length;i++){
        for(int j=0;j<matriz[0].length;j++){
            System.out.print(matriz[i][j]);
            System.out.print("|");
        }
        System.out.println("");
}
}
}
             
    
